numerals = ['M','D','C','L','X','V','I']

def checkNum(num):
	valid = False #variable used to see if a char in the num is a valid numeral
	for i in num: #goes through all of the chars in num
		valid = False #resets to false for each char
		for j in numerals: #goes through all of the numeral chars
			if i == j: #if the char is in the numerals array
				valid = True #the char is valid
		if valid == False: #otherwise, the number is invalid
			return False			

	for i in range(0,len(num)-1): #goes from start to second to last index
		for j in range(0, 7): #goes through numerals array
			if num[i] == numerals[j]: #gets the numeral the char in the num is
				if j%2 == 1: #if the char is a D, L, or V
					for k in range(0,j):
						if num[i+1] == numerals[k]:
							return False
					for k in range(i+1,len(num)): #goes from 1 past current char to the last char
						if num[i] == num[k]: #if the two chars are the same
							return False #not a valid Roman numeral
				else: #if the char is M, C, X, or I
					if j > 0: #if the numeral isn't M
						if len(num)-i >= 4: #if there are 4 or more 
							valid = False
							for k in range(len(num)-i):
								if num[k+i] != num[i]:
									valid = True
							if valid == False:
								return False
						#if the next char isn't the numeral before, 2 before, or the numeral		
						if num[i+1] != numerals[j] and num[i+1] != numerals[j-1] and num[i+1] != numerals[j-2]:
							if j != 6: #if the numeral isn't I
								#traverse the numerals backwards from current numeral
								for k in range(j,-1,-1): 
									#if the next char in the num is one of the bigger numerals
									if num[i+1] == numerals[k]:
										return False
							else: #if the numeral is I, invalid number
								return False
						if j == 6: #if the numeral is I
							#if the char is the last in the num and it doesn't equal the next char
							if i != len(num)-1 and num[i] != num[i+1]:
								if num[len(num)-1] == 'I': #if the last char equals I, the number is invalid
									return False		
						for k in range(i+1,len(num)):
							for l in range(0,j):
								if num[k] == numerals[l] and l < j-2:
									return False
								if k < len(num) and k > i+1:
									if num[k] == numerals[l]:
										return False
	return True #otherwise, valid number

def simplifyNum(num, index):
	#print(num)
	if index == len(num):
		return num
	done = True
	for i in range(index,len(num)):
		for j in range(len(numerals)):
			if num[i] == numerals[j]:
				for k in range(i,len(num)):
					for l in range(j):
						if num[k] == numerals[l]:
							done = False
							break
					if done == False:
						break
			if done == False:
				break
		if done == False:
			break
	if done == True:
		print(num)
		return num
	else:
		for i in range(7):
			if num[index] == numerals[i]:
				for j in range(i):
					if num[index+1] == numerals[j]: #if the next char is a bigger numeral
						if index != len(num)-2:
							num = num[:index+1] + numerals[j+1] + num[index+2:]
							num = num[:index] + num[index]*4 + num[index:]
							index += 4
							print("1")
						else:
							if j != 5:
								num = num[:index+1] + (num[index]*4) + numerals[j+1]
								index += 4
							else:
								num = num[:index+1] + (num[index]*3)
								index += 3
							print("2")
							#simplifyNum(num,index+1)
	return simplifyNum(num,index+1)
							

num1 = input("Enter the first number: ")
if checkNum(num1):
	print(simplifyNum(num1,0))
	num2 = input("Enter the number you want to subtract: ")
	if checkNum(num2):
		#subNum(num1, num2)
		print("Both numbers are good.")
	else:
		print("The Roman numeral is written incorrectly.")
else:
	print("The Roman numeral is written incorrectly.")