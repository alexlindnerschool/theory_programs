def f(n):
	if n > 100:
		return n-10
	else:
		return f(f(n+11))

print(f(1))
print(f(-6))
print(f(200))
print(f(27))
print(f(102))