import math

#parameter: int of digits that are going to be printed
def getDigits(digits):
	firstTwo = [0,1] #first nums of fib
	phi = ((1+math.sqrt(5)/2)) 
	print("|Start|End|Num Digits|Log2(Start)+2|LogPhi(Start)+2|")
	print("_"*70)
	for i in range(0, digits): #go through number of digits
		nextNum = (firstTwo[1]+firstTwo[0]) #gets next num
		firstTwo[0] = firstTwo[1] #moves to right
		firstTwo[1] = nextNum #moves next num to right
		lastNum = firstTwo[0] + firstTwo[1]-1 
		#prints the info for each digit
		print("| %7d | %7d | %10d | %13.1f | %15.1f |" %(nextNum, lastNum, (i+2), 
			round(math.log(nextNum, 2)), round(math.log(nextNum, phi))))

getDigits(30) 
