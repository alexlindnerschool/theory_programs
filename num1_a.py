#Creates a 2D array of an abacus with a value of nothing
def resetAbacus():
	abacus = [] #initialize the abacus
	for i in range(15):
		row = []
		for j in range(13):
			if i == 4: #if on row 4, make a line of dashes
				row.append('-')
			elif (i > 1 and i < 10): #if on a row without bead, add a space
				row.append(' ')
			else: #adds beads to original spots
				row.append('o')
		abacus.append(row) #adds row to abacus
	return abacus

#goes through the 2D abacus array(ab) and prints each character
def printAbacus(ab):
	for i in range(15):
		for j in range(13):
			print(ab[i][j], end =" ")
		print() #makes new line for next row

'''Parameters: passed a 2D array(abacus), a num value to put in the array, and the current column 
position
Purpose: return an abacus as a 2D array that represents the num originally passed in'''
def getAbacus(abacus, num, col):
	if num == 0: #base case
		return abacus #returns completed abacus with the value num
	else: #Go from left to right on abacus by column and change beads accordingly
		#TOP BEADS
		if num/(10**(12-col)*5) >= 1: #checks if num is divisible by bead value
			if num/(10**(12-col)*5) >= 2: #if num is divisible by bead twice
				abacus[0][col] = ' ' #remove first bead from top spot
				abacus[2][col] = 'o' #insert first bead at row of index 2 (row 3)
				num -= 10**(12-col)*5 #subtract from num when bead is added
			abacus[1][col] = ' '
			abacus[3][col] = 'o'
			num -= 10**(12-col)*5
		#BOTTOM BEADS (similar to top beads but with 5)
		if num/(10**(12-col)) >= 1: #checks if num is divisible by bead value at least once etc.
			if num/(10**(12-col)) >= 2:
				if num/(10**(12-col)) >= 3:
					if num/(10**(12-col)) >= 4:
						if num/(10**(12-col)) >= 5:
							abacus[14][col] = ' '
							abacus[9][col] = 'o'
							num -= 10**(12-col)
						abacus[13][col] = ' '
						abacus[8][col] = 'o'
						num -= 10**(12-col)
					abacus[12][col] = ' '
					abacus[7][col] = 'o'
					num -= 10**(12-col)
				abacus[11][col] = ' '
				abacus[6][col] = 'o'
				num -= 10**(12-col)
			abacus[10][col] = ' '
			abacus[5][col] = 'o'
			num -= 10**(12-col)
		getAbacus(abacus, num, col+1) #recursive call that moves the col to the right 1
	return abacus #returns abacus when finished with each call

'''Parameters: definition is passed two 2D arrays to add together(a1 and a2), a column 
index(col), and a carry number(carry)
Purpose: adds a2 to a1 by going from right to left by column in each abacus and adding the 
beads and, if necessary, use a carry number'''
def addAbacus(a1, a2, col, carry):
	if col < 0: #base case
		return a1 #return summed 2D array
	else:
		bottom = carry #bottom keeps track of number of beads the bottom needs (initially set to carry)
		carry = 0 #reset carry to 0
		top = 0 #top is reset to 0
		for i in range(5,10): #counts number of beads used in bottom row for each abacus
			if a1[i][col] == 'o':
				bottom += 1 #adds 1 to bottom if it finds a bead
			if a2[i][col] == 'o':
				bottom += 1
		if bottom >= 5: #checks if the bottom needs to carry to the top
			top += bottom//5 #adds number of necessary beads to top
		for i in range(2,4): #counts nymber of beads used in top row for each abacus
			if a1[i][col] == 'o':
				top += 1 #adds 1 to top if it finds a bead
			if a2[i][col] == 'o':
				top += 1
		if top >= 2: #checks if the top needs to carry to the next column
			carry = top//2 #sets the carry to the necessary number based off of the top number
			top = top%2 #gets number of top beads to be moved down
		remainders = bottom%5 #gets number of bottom beads to be moved up
		for i in range(5,15): #goes through the bottom bead rows
			if i-5 < remainders or i-5 >= remainders+5: #checks where there needs to be beads
				a1[i][col] = 'o' #add beads to the correct indices
			else:
				a1[i][col] = ' ' #add spaces where beads are unnecessary
		if top == 1: #if 1 bead is needed for top
			a1[0][col] = 'o'
			a1[1][col] = ' '
			a1[2][col] = ' '
			a1[3][col] = 'o'
		else: #if 0 beads are needed for top
			a1[0][col] = 'o'
			a1[1][col] = 'o'
			a1[2][col] = ' '
			a1[3][col] = ' '

		addAbacus(a1, a2, col-1, carry) #recursive call that moves one column to the left
	return a1 #returns 2D array (a1) at end of definition call

#gets input from user
num1 = ""
num2 = ""
done = False
expression = input("Enter expression: ")
for i in expression:
	if i != ' ' and i != '+' and done == False:
		num1 += i
	elif i != ' ' and i != '+' and done == True:
		num2 += i
	else:
		done = True

num1 = int(num1)
num2 = int(num2)
#gets the abacuses based off of user input
a1 = getAbacus(resetAbacus(), num1, 0)
a2 = getAbacus(resetAbacus(), num2, 0)

#prints out original inputs
printAbacus(a1)
print(num1)
print("*************************")
printAbacus(a2)
print(num2)
print("*************************")

#prints the added abacus
print("SUM (",num1,"+",num2,") =")
sumation = addAbacus(a1, a2, 12, 0)
printAbacus(sumation)
print(num1+num2)
print("*************************")