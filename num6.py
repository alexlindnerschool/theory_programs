import random

def minFirst(L, smallest, value, i, start):
	if L[i] < L[smallest]:
		smallest = i
	if i == len(L)-1:
		L[start] = L[smallest]
		L[smallest] = value
		return L
	return minFirst(L, smallest, value, i+1, start)

def selSort(L,i):
	if i == len(L)-1:
		return L
	else:
		L = minFirst(L,i,List[i],i,i)
		return selSort(L,i+1)

List = []
for i in range(500):
	List.append(random.randint(0,9))
print(selSort(List,0))